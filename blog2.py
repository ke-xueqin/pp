# 导入所需要的包
import requests
import json
import datetime
from time import strftime, sleep
# 请求头
head = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0'
}
# 地址。
url = 'https://j1.pupuapi.com/client/product/storeproduct/detail/7c1208da-907a-4391-9901-35a60096a3f9/30befb4c-1188-45c6-86b4-a2a9e490e976'
# 发起请求。
response = requests.get(url, headers=head)
# 将json格式的字符串转化成字典。
js_data = json.loads(response.text)
# 提取数据。
name = js_data['data']['name']
specs = js_data['data']['spec']
original_price = js_data['data']['market_price'] / 100
discount_price = (js_data['data']['price']) / 100
details = js_data['data']['share_content']
#观察数据。
print("-------------商品：" + name + "-------------")
print("规格：" + specs)
print("原价：" + str(original_price))
print("原价/折扣价：" + str(original_price) + "/" + str(discount_price))
print("详细内容：" + details)
print("-------------" + name + "的价格波动-------------")
while(1):
    response = requests.get(url,headers=head)
    js_data = json.loads(response.text)
    original_price = (js_data['data']['price'] ) * 0.01
# 获取时间
    time = datetime.datetime.now()
    print(str(time)+"价格为"+str(original_price))
# 隔10秒获取一次价格
    sleep(10)



